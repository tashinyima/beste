//
//  MovieViewController.m
//  Beste
//
//  Created by Tashi Nyima on 19/08/2016.
//  Copyright © 2016 AppCoda. All rights reserved.
//

#import "MovieViewController.h"
#import "APIManager.h"

#define apiURL [NSURL URLWithString: @"https://itunes.apple.com/gb/rss/topmovies/limit=100/genre=4401/json"]
#define globalQueue  dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
@interface MovieViewController ()

@end

@implementation MovieViewController

- (void)viewDidLoad
{
    APIManager *apiManager = [[APIManager alloc] init];
    
    [super viewDidLoad];
    
    
    dispatch_async(globalQueue, ^{
        
        NSData *data = [NSData dataWithContentsOfURL:apiURL];
        
        [apiManager performSelectorOnMainThread:@selector(dataRetrived:) withObject:data waitUntilDone:YES];
        
    });
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
