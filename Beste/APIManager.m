//
//  APIManager.m
//  Beste
//
//  Created by Tashi Nyima on 19/08/2016.
//  Copyright © 2016 AppCoda. All rights reserved.
//

#import "APIManager.h"

@implementation APIManager

-(void) dataRetrived: (NSData*) dataResponse
{
    NSError *error;
    
    NSDictionary *jsonRootDic = [NSJSONSerialization JSONObjectWithData:dataResponse options:kNilOptions error:&error];
    NSDictionary *feedDic = [jsonRootDic objectForKey:@"feed"];
    NSArray *entryArray = [feedDic objectForKey:@"entry"];
    
    self.movieTitle = [[NSMutableArray alloc] init];
    self.movieReleaseDates = [[NSMutableArray alloc] init];
    self.movieCategory = [[NSMutableArray alloc] init];
    self.movieDirectors = [[NSMutableArray alloc] init];
    self.movieImagesUrls = [[NSMutableArray alloc] init];
    self.movieSummary = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < entryArray.count; i++)
    {
        [self.movieTitle addObject:[[[entryArray objectAtIndex:i]
                                        objectForKey:@"im:name"]
                                        objectForKey:@"label"]];
        
        [self.movieReleaseDates addObject:[[[[entryArray objectAtIndex:i]
                                        objectForKey:@"im:releaseDate"]
                                        objectForKey:@"attributes"]
                                        objectForKey:@"label"]];
        
        [self.movieCategory addObject:[[[[entryArray objectAtIndex:i]
                                        objectForKey:@"category"]
                                        objectForKey:@"attributes"]
                                        objectForKey:@"term"]];
        
        [self.movieDirectors addObject:[[[entryArray objectAtIndex:i]
                                        objectForKey:@"im:artist"]
                                        objectForKey:@"label"]];
        [self.movieImagesUrls addObject:[[[[entryArray objectAtIndex:i]
                                        objectForKey:@"im:image"]
                                        objectAtIndex:2]
                                        objectForKey:@"label"]];
        
        id summaryObj = [[entryArray objectAtIndex:i] objectForKey:@"summary"];
        
        if (summaryObj)
        {
            [self.movieSummary addObject:[summaryObj objectForKey:@"label"]];
        }
    }
    
    NSLog(@"category:%@", self.movieCategory[0]);
}
@end
