//
//  main.m
//  Beste
//
//  Created by Tashi Nyima on 19/08/2016.
//  Copyright © 2016 AppCoda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
