//
//  APIManager.h
//  Beste
//
//  Created by Tashi Nyima on 19/08/2016.
//  Copyright © 2016 AppCoda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APIManager : NSObject

@property (strong, nonatomic) NSMutableArray *movieTitle;
@property (strong, nonatomic) NSMutableArray *movieDirectors;
@property (strong, nonatomic) NSMutableArray *movieSummary;
@property (strong, nonatomic) NSMutableArray *movieCategory;
@property (strong, nonatomic) NSMutableArray *movieImagesUrls;
@property (strong, nonatomic) NSMutableArray *movieReleaseDates;

-(void) dataRetrived: (NSData*) dataResponse;

@end
